const form = document.getElementById("form");
const email = document.getElementById("email");
const password = document.getElementById("password");
const errorMessageContainer = document.getElementById("errorMessage");

const url = "http://localhost:3000/login";

const handleFormSubmit = (e) => {
  e.preventDefault();
  if (email.value === "" && password.value === "") {
    errorMessageContainer.innerText = "이메일 혹은 패스워드를 입력해주세요.";
  }
  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email.value,
      password: password.value,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        return response.json().then((data) => {
          errorMessageContainer.innerText = data.error;
        });
      }
      window.location.href = "/";
    })
    .catch((error) => console.log("error", error));
};
form.addEventListener("submit", handleFormSubmit);
