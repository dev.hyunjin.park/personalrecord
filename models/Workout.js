import mongoose from "mongoose";
const Schema = mongoose.Schema;

const WorkoutSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  data: {
    type: Object,
    required: true,
    default: {},
  },
});

const Workout = mongoose.model("Workout", WorkoutSchema);
export default Workout;
