import { asyncHandler } from "../middlewares.js";
import User from "../models/User.js";
import bcrypt from "bcryptjs";

export const getHome = asyncHandler(async (req, res, next) => {
  const isLoggedIn = req.session?.isLoggedIn;
  const userEmail = req.session?.user?.email;
  // 유저 정보를 통으로 넘겨주는 게 좋은 생각일까?????
  if (!userEmail) {
    return res.render("home.pug");
  }
  const user = await User.findOne({ email: userEmail }).populate("workouts");
  if (!user) {
    return res
      .status(400)
      .json({ error: "로그인한 유저의 정보를 찾을 수 없습니다." });
  }
  res.render("home.pug", { user, isLoggedIn });
});

export const getJoin = (req, res) => {
  res.render("join.pug");
};
export const postJoin = asyncHandler(async (req, res, next) => {
  const { email, username, password } = req.body;
  const [emailExists, usernameExists] = await Promise.all([
    User.exists({ email }),
    User.exists({ username }),
  ]);
  if (emailExists) {
    return res.status(400).json({ error: "이미 존재하는 이메일입니다." });
  } else if (usernameExists) {
    return res.status(400).json({ error: "이미 존재하는 사용자명입니다." });
  }
  const hashedPassword = await bcrypt.hash(password, 5);
  const newUser = await User.create({
    email,
    username,
    password: hashedPassword,
  });
  if (!newUser) {
    return res.status(400).json({ error: "사용자 생성에 실패했습니다." });
  }
  res.status(200).json({ success: true });
});

export const getLogin = (req, res) => {
  res.render("login.pug");
};

export const postLogin = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    return res.status(400).json({ error: "존재하지 않는 이메일입니다." });
  }
  const ok = await bcrypt.compare(password, user.password);
  if (!ok) {
    return res.status(400).json({ error: "비밀번호가 일치하지 않습니다." });
  }
  req.session.isLoggedIn = true;
  req.session.user = user;
  // 세션 스토어에 유저를 기록한다
  return res.status(200).json({ success: true });
});
