import express from "express";
const router = express.Router();
import { postWorkout, getWorkout } from "../controllers/workoutController.js";

router.get("/", getWorkout);
router.post("/:userId", postWorkout);
export default router;
