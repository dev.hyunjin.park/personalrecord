const form = document.getElementById("form");
const email = document.getElementById("email");
const username = document.getElementById("username");
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirmPassword");
const errorContainer = document.getElementById("errorMessage");

const url = "http://localhost:3000/join";

const fetchData = (email, username, password) =>
  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      username,
      password,
    }),
  });

const handleSubmit = (e) => {
  e.preventDefault();
  if (password.value !== confirmPassword.value) {
    errorContainer.innerText = "비밀번호가 일치하지 않습니다.";
    return;
  } else if (password.value.length < 8) {
    errorContainer.innerText = "비밀번호가 너무 짧습니다.";
    return;
  }

  fetchData(email.value, username.value, password.value)
    .then((response) => {
      if (!response.ok) {
        return response.json().then((data) => {
          errorContainer.innerText = data.error;
        });
      } else {
        window.location.href = "/login";
      }
    })
    .catch((error) => console.log(error));
};

form.addEventListener("submit", handleSubmit);
