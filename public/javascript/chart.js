const canvas = document.getElementById("myChart");
const workoutData = canvas.getAttribute("data-workouts");
const parsedData = JSON.parse(workoutData);
const button = document.getElementById("button");
const totalWeights = [];
const personalRecords = [];
const dates = [];
let toggleButton = false;
let lineChart = null;

// workout 배열의 totalPerSet 값을 합산하는 함수
function getTotalPerSetSum(workoutData) {
  // workout 배열을 순회하며 totalPerSet 값을 추출하여 합산
  const sum = workoutData.workout.reduce((acc, item) => {
    return acc + parseInt(item.totalPerSet);
  }, 0);
  return sum;
}

// workout 배열의 weight 값 중 가장 큰 값을 구하는 함수
function getLargestWeight(workoutData) {
  // workout 배열의 weight 값들만 추출하여 숫자 배열로 변환
  const weights = workoutData.workout.map((item) => parseInt(item.weight));
  return Math.max(...weights);
}

// lb || kg 어떻게 보여줄 건지
parsedData.map((item) => {
  totalWeights.push(getTotalPerSetSum(item.data));
  personalRecords.push(getLargestWeight(item.data));
  dates.push(item.date);
});

console.log(totalWeights, personalRecords);

// total weight (LB) 그래프 데이터
const totalWeightData = {
  labels: dates.map((date) => date.split("T")[0]),
  datasets: [
    {
      label: "Total Weight (LB)",
      data: totalWeights,
      borderColor: "rgb(75, 192, 192)",
    },
  ],
};

const personalRecordData = {
  labels: dates.map((date) => date.split("T")[0]),
  datasets: [
    {
      label: "1RM (LB)",
      data: personalRecords,
      borderColor: "rgb(75, 192, 192)",
    },
  ],
};

// 그래프 옵션 정의
const options = {
  responsive: true,
  scales: {
    y: {
      beginAtZero: true,
    },
  },
};

function createChart(data) {
  if (lineChart) {
    // 이전에 생성된 차트가 있을 경우 파괴
    lineChart.destroy();
  }
  lineChart = new Chart(canvas, {
    type: "line",
    data,
    options: options,
  });
}

// 초기 그래프 생성
createChart(totalWeightData);

button.addEventListener("click", () => {
  toggleButton = !toggleButton;
  if (!toggleButton) {
    button.innerText = "1RM";
    createChart(totalWeightData);
  } else {
    button.innerText = "총 무게 합산";
    createChart(personalRecordData);
  }
});
