import express from "express";
const router = express.Router();
import {
  getHome,
  getJoin,
  postJoin,
  getLogin,
  postLogin,
} from "../controllers/userController.js";

router.get("/", getHome);
router.get("/join", getJoin);
router.post("/join", postJoin);
router.get("/login", getLogin);
router.post("/login", postLogin);

export default router;
