import { asyncHandler } from "../middlewares.js";
import User from "../models/User.js";
import Workout from "../models/Workout.js";

export const getWorkout = async (req, res) => {
  const userId = req.session?.user?.id;
  const user = await User.findOne({ id: userId }).populate("workouts");
  if (!user) {
    return res.status(400).json({ error: "유저를 찾을 수 없습니다." });
  }
  res.render("chart.pug", { data: user.workouts });
};

export const postWorkout = asyncHandler(async (req, res, next) => {
  const userId = req.params.userId;
  const { data } = req.body;
  const user = await User.findById(userId);
  if (!user) {
    return res.status(400).json({ error: "유저를 찾을 수 없습니다." });
  }
  const workout = await Workout.create({
    user: user.id,
    data,
  });
  if (!workout) {
    return res.status(400).json({ error: "운동을 저장하지 못했습니다." });
  }
  const exists = user.workoutNames.includes(data.workoutName);
  if (!exists) {
    user.workoutNames.push(data.workoutName);
  }
  user.workouts.push(workout);
  user.save();
  return res.status(200).json({ success: true, workout });
});
