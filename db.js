import dotenv from "dotenv";
dotenv.config();
import mongoose from "mongoose";

mongoose.set("strictQuery", false);
const url = process.env.MONGO_DB_URL;

const dbconnect = () => {
  mongoose
    .connect(url)
    .then(() => console.log("👍 MongoDB connected successfully"))
    .catch((error) => console.log("🥲 MongoDB error: ", error));
};

//connection 상태
mongoose.connection.on("error", (error) => {
  console.log("😇 🐌 Mongodb connection error", error);
});
mongoose.connection.on("disconnected", () => {
  console.log("🐌 Mongodb disconneced. Try reconnect....🐌 🐌 🐌 ");
});

export default dbconnect;
