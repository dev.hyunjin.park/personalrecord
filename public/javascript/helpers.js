export const createInput = (className, type, value) => {
  const input = document.createElement("input");
  input.setAttribute("class", className);
  input.setAttribute("type", type);
  input.setAttribute("value", value);
  return input;
};
export const createDiv = (className, innerText) => {
  const div = document.createElement("div");
  div.setAttribute("class", className);
  div.innerText = innerText;
  return div;
};
