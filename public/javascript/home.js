import { createInput, createDiv } from "../javascript/helpers.js";

const unit = document.getElementById("unit-select");
const table = document.querySelector("table");
const tbody = table.querySelector("tbody");
const tr = table.querySelectorAll("tr");
const addBtn = document.getElementById("add");
const removeBtn = document.getElementById("remove");
const weightInputs = document.querySelectorAll(".weight");
const repsInputs = document.querySelectorAll(".reps");
const checkboxes = document.querySelectorAll(".checkbox");
const result = document.getElementById("result");
const modal = document.getElementById("modal");
const modalContainer = document.getElementById("modal__container");
const modalContent = document.getElementById("modal__content");
const modalHeader = document.getElementById("modal__header");

const saveBtn = document.getElementById("save");
const workoutInput = document.getElementById("workoutInput");
const workoutSubmitBtn = document.getElementById("workoutSubmitBtn");
const workoutSelect = document.getElementById("workoutSelect");
const workoutDefaultOption = document.getElementById("workoutDefaultOption");
const undoBtn = document.getElementById("undo");

const CLASS_SHOW = "show";
const CLASS_NOTSHOW = "notShow";

const WEIGHT_UNIT = {
  LB: "LB",
  KG: "KG",
};

const TOTAL_RESULT = {
  getResultStr: function (number) {
    return `TOTAL RESULT
  ${number}${this.weightUnit} / ${this.getConversion(number)}`;
  },
  weightUnit: WEIGHT_UNIT.LB,
  changeUnit: function () {
    this.weightUnit =
      this.weightUnit === WEIGHT_UNIT.LB ? WEIGHT_UNIT.KG : WEIGHT_UNIT.LB;
  },
  getConversion: function (number) {
    if (this.weightUnit == WEIGHT_UNIT.LB) {
      return (number * 0.453592).toFixed(2) + "KG";
    } else {
      return (number * 2.204623).toFixed(2) + "LB";
    }
  },
};

// TOTAL RESULT의 문자열에서 첫번째 숫자만(선택된 무게 단위에 해당하는 총 무게 값) 반환
const getCurrentTotal = () => +result.innerText.match(/\d+/)[0];

const createTableRow = (
  count = 1,
  reps = 15,
  weight = 20,
  totalPerSet = 300
) => {
  const tr = document.createElement("tr");
  tbody.appendChild(tr);
  let tdList = "";
  tdList += createDiv("count", count);
  tdList += createInput("weight", "number", weight);
  tdList += createInput("reps", "number", reps);
  tdList += createDiv("totalPerSet", totalPerSet);
  tdList += createInput("checkbox", "checkbox");
  tr.innerHTML = tdList;
  const weightInputs = tr.querySelector(".weight");
  const repsInputs = tr.querySelector(".reps");
  const checkboxes = tr.querySelector(".checkbox");
  weightInputs.addEventListener("input", handleTotalWeightPerSet);
  repsInputs.addEventListener("input", handleTotalWeightPerSet);
  checkboxes.addEventListener("change", handleCheckboxes);
};

const handleAddBtn = () => {
  if (!tbody.lastElementChild) {
    // 테이블에 남은 리스트 요소가 없다면 모든 요소를 새로 생성한다
    createTableRow();
  } else {
    const lastElemCount =
      tbody.lastElementChild.querySelector(".count").innerText;
    createTableRow(lastElemCount);
  }
};

const handleRemoveBtn = () => {
  const lastChild = tbody.lastElementChild;
  const checkbox = lastChild.querySelector(".checkbox");
  const count = lastChild.querySelector(".count").innerText;
  const weight = lastChild.querySelector(".weight").value;
  const reps = lastChild.querySelector(".reps").value;
  const totalPerSet = +lastChild.querySelector(".totalPerSet").innerText;
  // 로컬스토리지에 배열로 저장한다
  if (checkbox.checked) {
    const data = localStorage.getItem("deletedList");
    let deletedList = data ? JSON.parse(data) : [];
    deletedList.push({ count, reps, weight, totalPerSet });
    localStorage.setItem("deletedList", JSON.stringify(deletedList));
  }
  if (result.innerText !== "" && checkbox.checked) {
    let currentTotal = getCurrentTotal();
    const totalResult = currentTotal - totalPerSet;
    result.innerText = TOTAL_RESULT.getResultStr(totalResult);
  }
  tbody.removeChild(lastChild);
};

// 로컬스토리지에 저장된 (삭제된) 리스트를 다시 복원시킨다
const handleUndoBtnClick = () => {
  const data = localStorage.getItem("deletedList");
  const parsed = JSON.parse(data);
  if (parsed?.length > 0) {
    const { count, reps, weight, totalPerSet } = parsed.pop();
    localStorage.setItem("deletedList", JSON.stringify(parsed));
    createTableRow(count, reps, weight, totalPerSet);
  } else {
    modalPops("체크된 후(✔️) 삭제된 항목이 없습니다.");
  }
};

// 한 세트당 총 무게를 구한다
const handleTotalWeightPerSet = (event) => {
  const parent = event.target.parentNode.parentNode;
  const weight = parent.querySelector(".weight");
  const reps = parent.querySelector(".reps");
  const totalPerSet = parent.querySelector(".totalPerSet");
  const checkbox = parent.querySelector(".checkbox");
  const total = +weight.value * +reps.value;
  const currentTotalPerSet = totalPerSet.innerText;
  totalPerSet.innerText = total;
  if (result.innerText !== "" && checkbox.checked) {
    let currentTotal = getCurrentTotal();
    const totalResult = currentTotal - currentTotalPerSet + total;
    result.innerText = TOTAL_RESULT.getResultStr(totalResult);
  }
};

const handleCheckboxes = (event) => {
  const tr = event.target.parentElement.parentElement;
  const totalPerSet = +tr.querySelector(".totalPerSet").innerText;
  const currentTotalResult = result?.innerText !== "" ? getCurrentTotal() : 0;
  let totalResult = "";
  if (event.target.checked) {
    totalResult = currentTotalResult + totalPerSet;
  } else {
    totalResult = currentTotalResult - totalPerSet;
  }
  result.innerText = TOTAL_RESULT.getResultStr(totalResult);
};

const unitChangeHandler = (event) => {
  const changedUnit = event.target.value;
  console.log(tr[0].childNodes[1]);
  tr[0].childNodes[1].innerText = changedUnit.toUpperCase();
  // TOTAL_RESULT.weightUnit = changedUnit.toUpperCase();
  if (result.innerText !== "") {
    TOTAL_RESULT.changeUnit();
    const newString = TOTAL_RESULT.getResultStr(getCurrentTotal());
    result.innerText = newString;
  }
};

const modalPops = (str) => {
  modalContent.innerText = str;
  modal.className = "show";
  modalContainer.className = "show";
  modalContent.className = "show";
  modalHeader.className = "show";
  const icon = modalHeader.querySelector(".icon");
  const handleIconClick = () => {
    modal.className = "notShow";
    modalContainer.className = "notShow";
    modalContent.className = "notShow";
    modalHeader.className = "notShow";
    icon.removeEventListener("click", handleIconClick);
  };
  icon.addEventListener("click", handleIconClick);
};

const handleSaveBtnClick = (e) => {
  const isLoggedIn = e.target.getAttribute("data-isLoggedIn");
  const userId = e.target.getAttribute("data-userId");
  if (!isLoggedIn) {
    modalPops("저장 기능은 회원가입 후 이용하실 수 있습니다.");
    // to do : 회원가입 or 로그인 버튼
  }
  const allCheckboxes = document.querySelectorAll(".checkbox");
  console.log(allCheckboxes);
  const checked = [...allCheckboxes].filter((item) => item.checked);
  const selectedWorkoutOption =
    workoutSelect.options[workoutSelect.selectedIndex];

  if (checked.length === 0) {
    return modalPops("완료된 운동이 없습니다");
  } else if (selectedWorkoutOption.id === "workoutDefaultOption") {
    return modalPops("운동 종목을 선택해주세요");
  }
  // 종목이름: selectedWorkoutOption
  // 체크된 항목 객체로 저장
  // checkboxes[0].parentNode.parentNode: 리스트
  // 1: weight, 2: reps, 3: totalPerSet
  const workout = [];
  [...allCheckboxes].map((item) => {
    if (item.checked) {
      parent = item.parentNode.parentNode;
      const weight = parent.querySelector(".weight").value;
      const reps = parent.querySelector(".reps").value;
      const totalPerSet = parent.querySelector(".totalPerSet").innerText;
      const set = { weight, reps, totalPerSet };
      workout.push(set);
    }
  });
  fetch(`http://localhost:3000/workout/${userId}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userId,
      data: {
        unit: unit.value,
        workoutName: selectedWorkoutOption.value,
        workout,
      },
    }),
  })
    .then((result) => result.json())
    .then((data) => {
      console.log(data);
      // 성공시 운동 상세 페이지로 이동
      // fetch 실패시 에러 메세지 표시
      if (localStorage.getItem("deletedList")) {
        localStorage.removeItem("deletedList");
      }
    });
};

const handleWorkoutSubmitBtn = () => {
  // TO DO: DB에 유저가 추가한 option이 이미 있다면 default value로 설정하기
  if (workoutInput.value !== "") {
    workoutInput.parentElement.classList.replace(CLASS_SHOW, CLASS_NOTSHOW);
    workoutSelect.parentElement.classList.replace(CLASS_NOTSHOW, CLASS_SHOW);
    // 만약 workoutInput.value가 이미 옵션 목록에 존재한다면 생성하지 않는다
    const options = workoutSelect.querySelectorAll("option");
    let existingOption = null;
    [...options].map((option) =>
      option.value === workoutInput.value ? (existingOption = option) : null
    );
    if (existingOption) {
      return (existingOption.selected = true);
    }
    const option = document.createElement("option");
    const date = new Date().getTime();
    option.id = `workoutOption-${date}`;
    option.value = workoutInput.value;
    option.innerText = workoutInput.value;
    option.selected = true;
    workoutSelect.insertBefore(option, workoutDefaultOption);
  }
};

const handleWorkoutSelect = () => {
  if (workoutDefaultOption.selected) {
    workoutInput.parentElement.classList.replace(CLASS_NOTSHOW, CLASS_SHOW);
    workoutSelect.parentElement.classList.replace(CLASS_SHOW, CLASS_NOTSHOW);
  }
};

addBtn.addEventListener("click", handleAddBtn);
removeBtn.addEventListener("click", handleRemoveBtn);
undoBtn.addEventListener("click", handleUndoBtnClick);
[...weightInputs].map((item) =>
  item.addEventListener("input", handleTotalWeightPerSet)
);
[...repsInputs].map((item) =>
  item.addEventListener("input", handleTotalWeightPerSet)
);
[...checkboxes].map((checkbox) =>
  checkbox.addEventListener("change", handleCheckboxes)
);
unit.addEventListener("change", unitChangeHandler);
workoutSubmitBtn?.addEventListener("click", handleWorkoutSubmitBtn);
workoutSelect?.addEventListener("change", handleWorkoutSelect);
saveBtn.addEventListener("click", (e) => handleSaveBtnClick(e));
