import dotenv from "dotenv";
dotenv.config();
import dbconnect from "./db.js";
import express from "express";
import morgan from "morgan";
import session from "express-session";
import MongoStore from "connect-mongo";
import cookieParser from "cookie-parser";
import createError from "http-errors";
import cors from "cors";
const app = express();
const port = 3000;
dbconnect();
import { globalMiddleware } from "./middlewares.js";
import indexRouter from "./routers/indexRouter.js";
import workoutRouter from "./routers/workoutRouter.js";

app.use(cors());
app.set("view engine", "pug");
app.set("views", "./views");
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static("public"));
// 모든 경로에서 "public" 디렉토리의 정적 파일을 제공하겠다
app.use(
  session({
    secret: process.env.COOKIE_SECRET,
    resave: false,
    saveUninitialized: false,
    store: MongoStore.create({
      mongoUrl: process.env.MONGO_DB_URL,
    }),
    cookie: {
      maxAge: 7 * 24 * 60 * 60 * 1000, //만료되면 로그아웃!!
    },
  })
);

app.use("/", globalMiddleware, indexRouter);
app.use("/workout", globalMiddleware, workoutRouter);

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({ result: "fail", error: err.message });
});
app.use((req, res, next) => {
  next(createError(404));
});
app.listen(port, () => {
  console.log(`서버가 http://localhost:${port} 에서 실행 중입니다.`);
});

export default app;
