const table = document.querySelector(".container table");

const getLocalStorage = () => {
  const storage = JSON.parse(window.localStorage.getItem("result"));
  if (storage) {
    const tr = document.createElement("tr");
    table.appendChild(tr);
    const td = document.createElement("td");
    const td2 = document.createElement("td");
    const td3 = document.createElement("td");
    tr.appendChild(td);
    tr.appendChild(td2);
    tr.appendChild(td3);
    td.innerText = "종목";
    td2.innerText = "LB";
    td3.innerText = "KG";

    storage.map((item) => {
      console.log(item);
      const tr = document.createElement("tr");
      table.appendChild(tr);
      const eventName = document.createElement("td");
      const finalResult_lb = document.createElement("td");
      const finalResult_kg = document.createElement("td");
      eventName.innerText = item.eventName;
      finalResult_lb.innerText = item.finalResult_lb;
      finalResult_kg.innerText = item.finalResult_kg;
      tr.appendChild(eventName);
      tr.appendChild(finalResult_lb);
      tr.appendChild(finalResult_kg);
    });
  } else {
    document.write("데이터가 없습니다");
  }
};

getLocalStorage();
